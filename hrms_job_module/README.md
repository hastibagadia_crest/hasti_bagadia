## React HandsOn Cron Job Liting wireframes are below.

- - -

+ ####  Listing View

![List](./src/assets/wireframes/List.png)

- - -

+ ####  Edit View

![Edit](./src/assets/wireframes/Edit.png)

- - -

+ ####  Delete View

![Delete](./src/assets/wireframes/Delete.png)

- - -

+ ####  Run View

![Run](./src/assets/wireframes/Run.png)

- - -

+ ####  Multi Select View

![Multi Select](./src/assets/wireframes/MultiSelect.png)
