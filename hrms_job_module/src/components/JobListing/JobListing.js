import React, { useState } from "react";
import { connect } from "react-redux";
import { Table, Space, Tag, Button } from "antd";
import Moment from "moment";

import {
  DURATION_MAP,
  TIME_STRING,
  runMultiAction,
  deleteMultiAction,
} from "../../store/action";
import CustomAlertModal from "../UI/CustomAlertModal/CustomAlertModal";
import CustomEditModal from "../UI/CustomEditModal/CustomEditModal";

import classes from "./JobListing.module.css";

const COLUMNS = [
  {
    title: "Id",
    dataIndex: "id",
    key: "id",
    align: "center",
  },
  {
    title: "Title",
    dataIndex: "title",
    key: "title",
    align: "center",
    sorter: (a, b) => a.title.length - b.title.length,
  },
  {
    title: "Type",
    dataIndex: "type",
    key: "type",
    align: "center",
    render: (text, record) => {
      let color = "cyan"
      if(text === 'custom') color = "gold"
    return <Tag color={color}>{text}</Tag>},
  },
  {
    title: "Recurrence",
    dataIndex: "recurrence",
    key: "recurrence",
    align: "center",
    render: (text, record) => {
      if (record.type === "predefined") return <Tag color="green">{text}</Tag>;
      else if (record.type === "custom")
        return (
          <Tag color="green">
            {Moment(record.recurrence, TIME_STRING).format("DD-MM-YYYY")}
          </Tag>
        );
    },
  },
  {
    title: "Next Execution",
    key: "nextExecution",
    align: "center",
    render: (text, record) => {
      if (record.type === "predefined") {
        if (record.lastExecution.length === 0)
          return Moment(record.creationTime, TIME_STRING)
            .add(1, DURATION_MAP[record.recurrence])
            .format("DD-MM-YYYY (LT)");
        else
          return Moment(record.lastExecution, TIME_STRING)
            .add(1, DURATION_MAP[record.recurrence])
            .format("DD-MM-YYYY (LT)");
      } else if (record.type === "custom")
        return Moment(record.recurrence, TIME_STRING).format("DD-MM-YYYY (LT)");
    },
  },
  {
    title: "Last Execution",
    dataIndex: "lastExecution",
    key: "lastExecution",
    align: "center",
    // render: (text, record) => Moment(record.lastExecution, TIME_STRING).from(Moment()),
    render: (text, record) => {
      if (record.lastExecution.length === 0) return "Not Yet Executed";
      else
        return Moment(record.lastExecution, TIME_STRING).format(
          "DD-MM-YY (LT)"
        );
    },
  },
  {
    title: "Action",
    key: "action",
    align: "center",
    render: (text, record) => (
      <Space size="small">
        <CustomEditModal
          recurrence={record.recurrence}
          jobId={record.id}
          jobType={record.type}
        />
        <CustomAlertModal
          type="Run"
          color="lightblue"
          jobName={record.title}
          jobId={record.id}
        />
        <CustomAlertModal
          type="Delete"
          color="salmon"
          jobName={record.title}
          jobId={record.id}
        />
      </Space>
    ),
  },
];

const JobListing = (props) => {
  const [selectedItems, setSelectedItems] = useState([]);

  const rowSelection = {
    onChange: (selectedRowKeys) => {
      if (selectedRowKeys.length !== 0) {
        setSelectedItems(Object.values(selectedRowKeys));
      } else if (selectedRowKeys.length === 0) {
        setSelectedItems([]);
      }
    },
  };

  const runMultileJobHandler = () => {
    props.onMultiRun(runMultiAction(selectedItems));
  };

  const deleteMultileJobHandler = () => {
    props.onMultiDelete(deleteMultiAction(selectedItems));
  };

  let multiSelectOperations = null;
  if (selectedItems.length !== 0) {
    multiSelectOperations = (
      <Space size="middle" style={{ margin: "5px 0 2px 10px" }}>
        <Button
          type="text"
          style={{ fontWeight: "bolder", color: "lightblue" }}
          onClick={runMultileJobHandler}
        >
          Run Selected
        </Button>
        <Button
          type="text"
          style={{ fontWeight: "bolder", color: "salmon" }}
          onClick={deleteMultileJobHandler}
        >
          Delete Selected
        </Button>
      </Space>
    );
  }

  return (
    <div>
      {multiSelectOperations}
      <Table
        dataSource={props.data}
        columns={COLUMNS}
        rowSelection={{ ...rowSelection }}
        className={classes.JobListing}
        rowKey="id"
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    data: state.dataSource.filter(item => item.deleted === false),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onMultiRun: (runMultiObject) => dispatch(runMultiObject),
    onMultiDelete: (deleteMultiObject) => dispatch(deleteMultiObject),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(JobListing);
