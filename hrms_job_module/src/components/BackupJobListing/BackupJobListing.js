import React, { useState } from "react";
import { connect } from "react-redux";
import { Table, Tag, Button } from "antd";
import Moment from "moment";

import CustomResponseModal from "../UI/CustomRestoreModal/CustomRestoreModal"

import {
  DURATION_MAP,
  TIME_STRING,
  restoreMultiAction
} from "../../store/action";
import classes from "./BackupJobListing.module.css";

const COLUMNS = [
  {
    title: "Id",
    dataIndex: "id",
    key: "id",
    align: "center",
  },
  {
    title: "Title",
    dataIndex: "title",
    key: "title",
    align: "center",
    sorter: (a, b) => a.title.length - b.title.length,
  },
  {
    title: "Type",
    dataIndex: "type",
    key: "type",
    align: "center",
    render: (text, record) => {
      let color = "cyan";
      if (text === "custom") color = "gold";
      return <Tag color={color}>{text}</Tag>;
    },
  },
  {
    title: "Recurrence",
    dataIndex: "recurrence",
    key: "recurrence",
    align: "center",
    render: (text, record) => {
      if (record.type === "predefined") return <Tag color="green">{text}</Tag>;
      else if (record.type === "custom")
        return (
          <Tag color="green">
            {Moment(record.recurrence, TIME_STRING).format("DD-MM-YYYY")}
          </Tag>
        );
    },
  },
  {
    title: "Next Execution",
    key: "nextExecution",
    align: "center",
    render: (text, record) => {
      if (record.type === "predefined") {
        if (record.lastExecution.length === 0)
          return Moment(record.creationTime, TIME_STRING)
            .add(1, DURATION_MAP[record.recurrence])
            .format("DD-MM-YYYY (LT)");
        else
          return Moment(record.lastExecution, TIME_STRING)
            .add(1, DURATION_MAP[record.recurrence])
            .format("DD-MM-YYYY (LT)");
      } else if (record.type === "custom")
        return Moment(record.recurrence, TIME_STRING).format("DD-MM-YYYY (LT)");
    },
  },
  {
    title: "Last Execution",
    dataIndex: "lastExecution",
    key: "lastExecution",
    align: "center",
    render: (text, record) => {
      if (record.lastExecution.length === 0) return "Not Yet Executed";
      else
        return Moment(record.lastExecution, TIME_STRING).format(
          "DD-MM-YY (LT)"
        );
    },
  },
  {
    title: "Action",
    key: "action",
    align: "center",
    render: (text, record) => <CustomResponseModal jobId={record.id} jobName={record.title}/>,
  },
];

const BackupJobListing = (props) => {
  const [selectedItems, setSelectedItems] = useState([]);

  const rowSelection = {
    onChange: (selectedRowKeys) => {
      if (selectedRowKeys.length !== 0) {
        setSelectedItems(Object.values(selectedRowKeys));
      } else if (selectedRowKeys.length === 0) {
        setSelectedItems([]);
      }
    },
  };

  const restoreMultileJobHandler = () => {
    props.onMultiRestore(restoreMultiAction(selectedItems));
  };

  let multiSelectOperations = null;
  if (selectedItems.length !== 0) {
    multiSelectOperations = (
      <Button
        type="text"
        style={{
          fontWeight: "bolder",
          color: "#6666ff",
          margin: "5px 0 2px 10px",
        }}
        onClick={restoreMultileJobHandler}
      >
        Restore Selected
      </Button>
    );
  }

  return (
    <div>
      {multiSelectOperations}
      <Table
        dataSource={props.data}
        columns={COLUMNS}
        rowSelection={{ ...rowSelection }}
        className={classes.BackupJobListing}
        rowKey="id"
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    data: state.dataSource.filter((item) => item.deleted === true),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onMultiRestore: (restoreMultiObject) => dispatch(restoreMultiObject),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BackupJobListing);
