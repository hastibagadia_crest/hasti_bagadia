import React from "react";
import { connect } from "react-redux";
import { Form, Input, Checkbox, DatePicker, TimePicker, Space } from "antd";

import { createNewCroneJob } from "../../../store/action";

const initialState = {
  title: "",
  file: "",
  date: "",
  time: "",
  runNow: false
};

class CustomdJob extends React.Component {
  state = { ...initialState };

  onInputChange = (event) => {
    this.setState({ title: event.target.value });
  };

  onScriptSelect = (event) => {
    console.log(event.target.files[0]["name"])
    this.setState({ file: event.target.files[0] });
  };

  onDateChange = (date) => {
    this.setState({ date: date.format("DD-MM-YY") });
  };

  onTimeChange = (time) => {
    this.setState({ time: time.format("hh:mm:ss") });
  };

  onRunNowChange = (event) => {
      this.setState({runNow: event.target.checked})
  }

  resetState = () => {
    this.setState({ ...initialState });
  };

  onSubmitHandler = () => {
    this.props.onSubmit(
      createNewCroneJob({
        title: this.state.title,
        file: this.state.file,
        type: "custom",
        recurrence: `${this.state.date} ${this.state.time}`,
      }, this.state.runNow)
    );
    this.setState({...initialState})
  };

  render() {
    return (
      <Form labelCol={{ span: 6 }} wrapperCol={{ span: 20 }}>
        <Form.Item label="Title">
          <Input
            placeholder="Enter Title"
            value={this.state.title}
            onChange={this.onInputChange}
          />
        </Form.Item>
        <Form.Item label="Choose Script File">
          <input type="file" onChange={this.onScriptSelect}/>
        </Form.Item>
        <Form.Item label="When to run">
          <Space size="middle">
          <DatePicker onChange={this.onDateChange} format="DD-MM-YY" />
          <TimePicker onChange={this.onTimeChange} format="hh:mm:ss"/>
          </Space>
        </Form.Item>
        <Form.Item>
            <Checkbox onChange={this.onRunNowChange} checked={this.state.runNow}>Run Now</Checkbox>
        </Form.Item>
      </Form>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onSubmit: (cronJobObject) => dispatch(cronJobObject),
  };
};

export default connect(null, mapDispatchToProps, null, {'forwardRef':true})(CustomdJob);
