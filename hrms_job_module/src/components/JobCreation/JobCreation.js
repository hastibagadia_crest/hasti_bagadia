import React from "react";

import { Button, Modal, Radio, Row, Col, message } from "antd";
import { PlusOutlined } from "@ant-design/icons";

import PredefinedJob from "./PredefinedJob/PredefinedJob";
import CustomdJob from "./CustomJob/CustomJob";

class JobCreation extends React.Component {
  state = {
    jobType: "predefined",
    showModal: false,
  };

  okHandler = () => {
    this.setState({ showModal: false });
    if(this.state.jobType === 'predefined')
        this.refs.predifined.onSubmitHandler()
    else if(this.state.jobType === 'custom')
        this.refs.custom.onSubmitHandler()
    message.success(`New ${this.state.jobType} job is scheduled.`)
    this.setState({jobType: 'predefined'})
  };

  cancelHandler = () => {
    this.setState({ showModal: false });
    if(this.state.jobType === 'predefined')
        this.refs.predifined.resetState()
    else if(this.state.jobType === 'custom')
        this.refs.custom.resetState()
    this.setState({jobType: 'predefined'})
  };

  onJobTypeChange = (event) => {
    this.setState({ jobType: event.target.value });
  };

  render() {
    let content = <PredefinedJob ref="predifined"/>;
    if(this.state.jobType === 'custom') content = <CustomdJob ref="custom"/>

    return (
      <React.Fragment>
        <Button
          type="primary"
          style={{ margin: "5px 0 2px 20px" }}
          onClick={() => {
            this.setState({ showModal: true });
          }}
        >
          <PlusOutlined /> Create Job
        </Button>
        <Modal
          width={600}
          title="Schudele Job"
          visible={this.state.showModal}
          onOk={this.okHandler}
          onCancel={this.cancelHandler}
          footer={[
            <Button key="back" onClick={this.cancelHandler}>
              Back
            </Button>,
            <Button key="schedule" type="primary" onClick={this.okHandler}>
              Schedule
            </Button>,
          ]}
        >
          <Row gutter={[0,20]}>
            <Col span={24}>
              <Radio.Group
                onChange={this.onJobTypeChange}
                value={this.state.jobType}
              >
                <Radio value="predefined">Predefined</Radio>
                <Radio value="custom">Custom</Radio>
              </Radio.Group>
            </Col>
          </Row>
          <Row gutter={[0,20]}>
            <Col span={24}>{content}</Col>
          </Row>
        </Modal>
      </React.Fragment>
    );
  }
}

export default JobCreation;
