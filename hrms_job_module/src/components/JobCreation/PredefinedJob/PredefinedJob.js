import React from "react";
import { connect } from "react-redux";
import { Form, Input, Select, Checkbox } from "antd";

import { CHRONE_JOB_LIST } from "../../../FakeData/Fakedata";
import { DURATION_MAP } from "../../../store/action";
import { createNewCroneJob } from "../../../store/action";

const initialState = {
  title: "",
  script: "",
  recurrence: "Every Hour",
  runNow: false
};

class PredefinedJob extends React.Component {
  state = { ...initialState };

  onInputChange = (event) => {
    this.setState({ title: event.target.value });
  };

  onJobSelect = (value) => {
    this.setState({ script: value });
  };

  onRecureenceSelect = (value) => {
    this.setState({ recurrence: value });
  };

  onRunNowChange = (event) => {
      this.setState({runNow: event.target.checked})
  }

  resetState = () => {
    this.setState({ ...initialState });
  };

  onSubmitHandler = () => {
    this.props.onSubmit(
      createNewCroneJob({
        title: this.state.title,
        script: this.state.script,
        type: "predefined",
        recurrence: this.state.recurrence,
      }, this.state.runNow)
    );
    this.setState({...initialState})
  };

  render() {
    return (
      <Form labelCol={{ span: 5}} wrapperCol={{ span: 20 }}>
        <Form.Item label="Title">
          <Input
            placeholder="Enter Title"
            value={this.state.title}
            onChange={this.onInputChange}
          />
        </Form.Item>
        <Form.Item label="Select Script">
          <Select
            onChange={this.onJobSelect}
            value={this.state.script}
          >
            {CHRONE_JOB_LIST.map((job, index) => {
              return (
                <Select.Option key={index} value={job}>
                  {job}
                </Select.Option>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item label="Recurrence">
          <Select
            placeholder="Select Recurrece"
            onChange={this.onRecureenceSelect}
            value={this.state.recurrence}
          >
            {Object.keys(DURATION_MAP).map((duration) => {
              return (
                <Select.Option value={duration} key={duration}>
                  {duration}
                </Select.Option>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item>
            <Checkbox onChange={this.onRunNowChange} checked={this.state.runNow}>Run Now</Checkbox>
        </Form.Item>
      </Form>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onSubmit: (cronJobObject) => dispatch(cronJobObject),
  };
};

export default connect(null, mapDispatchToProps, null, {'forwardRef':true})(PredefinedJob);
