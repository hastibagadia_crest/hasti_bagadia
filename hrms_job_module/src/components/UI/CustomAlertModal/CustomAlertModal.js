import React from "react";
import { Button, Modal, Tooltip, message } from "antd";
import { connect } from "react-redux";
import { PlaySquareFilled, DeleteFilled } from "@ant-design/icons";

import { runAction, deleteAction } from "../../../store/action";

class CustomAlertModal extends React.Component {
  state = {
    showModal: false,
  };

  okHandler = () => {
    this.setState({ showModal: false });
    if (this.props.type === "Run") {
      this.props.onRunHanlder(runAction(this.props.jobId));
      message.success(`${this.props.jobName} is running`)
    } else {
      this.props.onDeleteHanlder(deleteAction(this.props.jobId));
      message.error(`${this.props.jobName} is deleted`)
    }
  };

  cancelHandler = () => {
    this.setState({ showModal: false });
  };

  render() {
    const { type, color, jobName } = this.props;
    let buttonName = null;
    if (type === "Run") buttonName = <PlaySquareFilled />;
    else if (type === "Delete") buttonName = <DeleteFilled />;
    return (
      <React.Fragment>
        <Button
          type="text"
          style={{ fontWeight: "bolder", color: `${color}` }}
          onClick={() => {
            this.setState({ showModal: true });
          }}
        >
          <Tooltip title={type} placement="bottomLeft">{buttonName}</Tooltip>
        </Button>
        <Modal
          title={`${type} Alert !`}
          visible={this.state.showModal}
          onOk={this.okHandler}
          onCancel={this.cancelHandler}
        >
          Do you want to {type} <strong>" {jobName} "</strong> job ?
        </Modal>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onRunHanlder: (runActionObject) => dispatch(runActionObject),
    onDeleteHanlder: (deleteActionObject) => dispatch(deleteActionObject),
  };
};

export default connect(null, mapDispatchToProps)(CustomAlertModal);
