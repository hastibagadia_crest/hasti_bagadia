import React from "react";
import { Button, Modal, Tooltip, message } from "antd";
import { connect } from "react-redux";
import { CopyFilled } from "@ant-design/icons";

import { restoreAction } from "../../../store/action";

class CustoRestoreModal extends React.Component {
  state = {
    showModal: false,
  };

  okHandler = () => {
    this.setState({ showModal: false });
    this.props.onRestoreHanlder(restoreAction(this.props.jobId));
    message.success(`${this.props.jobName} is restored`);
  };

  cancelHandler = () => {
    this.setState({ showModal: false });
  };

  render() {
    const { jobName } = this.props;
    return (
      <React.Fragment>
        <Button
          type="text"
          style={{ fontWeight: "bolder", color: "#6666ff" }}
          onClick={() => {
            this.setState({ showModal: true });
          }}
        >
          <Tooltip title="Restore" placement="bottomLeft">
            <CopyFilled />
          </Tooltip>
        </Button>
        <Modal
          title={"Restore Alert !"}
          visible={this.state.showModal}
          onOk={this.okHandler}
          onCancel={this.cancelHandler}
        >
          Do you want to Restore <strong>" {jobName} "</strong> job ?
        </Modal>
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onRestoreHanlder: (restoreActionObject) => dispatch(restoreActionObject),
  };
};

export default connect(null, mapDispatchToProps)(CustoRestoreModal);
