import React from "react";
import { connect } from "react-redux";
import { Button, Modal, DatePicker, TimePicker, Select, Space, Tooltip, message } from "antd";
import { EditFilled } from "@ant-design/icons"
import Moment from "moment";

import {
  TIME_STRING,
  editCustomAction,
  editPredefinedAction,
} from "../../../store/action";

const { Option } = Select;

class CustomEditModal extends React.Component {
  state = {
    recurrence: this.props.recurrence,
    date: Moment(this.props.dataItem.recurrence, TIME_STRING),
    time: Moment(this.props.dataItem.recurrence, TIME_STRING),
    showModal: false,
  };

  okHandler = () => {
    this.setState({ showModal: false });
    if (this.props.jobType === "predefined") {
      this.props.editHandler(
        editPredefinedAction(this.props.jobId, this.state.recurrence)
      );
    } else if (this.props.jobType === "custom") {
      this.props.editHandler(
        editCustomAction(
          this.props.jobId,
          this.state.date.format("DD-MM-YYYY"),
          `${this.state.date.format("DD-MM-YYYY")} ${this.state.time.format(
            "hh:mm:ss"
          )}`
        )
      );
    }
    message.success(`${this.props.dataSource} job details are edited`)
  };

  cancelHandler = () => {
    this.setState({ showModal: false });
  };

  onDateChange = (date) => {
    this.setState({ date: date });
  };

  onTimeChange = (time) => {
    this.setState({ time: time });
  };

  onRecurrenceChange = (value) => {
    this.setState({ recurrence: value });
  };

  render() {
    let content = null;

    if (this.props.jobType === "custom") {
      content = (
        <Space size="large" align="center">
          Date : 
          <DatePicker
            defaultValue={Moment(this.state.date, TIME_STRING)}
            format="DD-MM-YYYY"
            onChange={this.onDateChange}
          />
          Time :
          <TimePicker
            defaultValue={Moment(this.state.time, TIME_STRING)}
            format="hh:mm:ss"
            onChange={this.onTimeChange}
          />
        </Space>
      );
    } else if (this.props.jobType === "predefined") {
      content = (
        <div>
          Recurrence :{" "}
          <Select
            style={{ width: "200px" }}
            placeholder="Select Recurrence"
            onChange={this.onRecurrenceChange}
            defaultValue={this.state.recurrence}
          >
            <Option value="Every Hour">Every Hour</Option>
            <Option value="Every Day">Every Day</Option>
            <Option value="Every Week">Every Week</Option>
            <Option value="Every Month">Every Month</Option>
            <Option value="Every Year">Every Year</Option>
          </Select>
        </div>
      );
    }

    return (
      <React.Fragment>
        <Button
          type="text"
          style={{ fontWeight: "bolder", color: "green" }}
          onClick={() => {
            this.setState({ showModal: true });
          }}
        >
          <Tooltip title="Edit" placement="bottomLeft"><EditFilled /></Tooltip>
        </Button>
        <Modal
          title="Edit"
          visible={this.state.showModal}
          onOk={this.okHandler}
          onCancel={this.cancelHandler}
        >
          {content}
        </Modal>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    dataItem: state.dataSource.find((item) => item.id === ownProps.jobId),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    editHandler: (editedObject) => dispatch(editedObject),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomEditModal);
