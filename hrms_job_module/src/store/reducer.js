import {
  RUN,
  RUN_MULTI,
  DELETE_MULTI,
  DELETE,
  RESTORE,
  RESTORE_MULTI,
  EDIT_PREDEFINED,
  EDIT_CUSTOM,
  TIME_STRING,
  CREATE_NEW
} from "./action";
import Moment from "moment";

const initialState = {
  dataSource: [
    {
      id: "1591936534645",
      title: "Clean up old process handles",
      type: "predefined",
      file: 'Cron Job 1',
      recurrence: "Every Day",
      creationTime: "09-06-2020 09:52:33",
      lastExecution: "09-06-2020 12:00:00",
      deleted: false
    },
    {
      id: "1591936538739",
      title: "Clean users from database",
      type: "predefined",
      file: 'Cron Job 2',
      recurrence: "Every Hour",
      creationTime: "10-06-2020 08:21:33",
      lastExecution: "10-06-2020 10:21:33",
      deleted: false
    },
    {
      id: "1591936542153",
      title: "Employee sync in HRMS",
      type: "custom",
      file: 'Cron Job 3',
      recurrence: "25-06-2020 12:00:00",
      creationTime: "05-06-2020 08:21:33",
      lastExecution: "05-06-2020 10:21:33",
      deleted: false
    },
    {
      id: "1591936544140",
      title: "Clean users from database",
      type: "predefined",
      file: 'Cron Job 4',
      recurrence: "Every Week",
      creationTime: "10-06-2020 08:21:33",
      lastExecution: "10-06-2020 10:21:33",
      deleted: false
    },
    {
      id: "1591936546172",
      title: "Clean users from database",
      type: "predefined",
      file: 'Cron Job 5',
      recurrence: "Every Month",
      creationTime: "10-06-2020 08:21:33",
      lastExecution: "10-06-2020 10:21:33",
      deleted: false
    },
    {
      id: "1591936547053",
      title: "Clean users from database",
      type: "predefined",
      file: 'Cron Job 6',
      recurrence: "Every Year",
      creationTime: "10-06-2020 08:21:33",
      lastExecution: "10-06-2020 10:21:33",
      deleted: false
    },
  ],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case RUN:
      let updatedDataSourceRun = [...state.dataSource];
      const dataItemIndexRun = updatedDataSourceRun.findIndex(
        (item) => item.id === action.jobId
      );
      updatedDataSourceRun[dataItemIndexRun].lastExecution = Moment().format(
        TIME_STRING
      );
      return {
        ...state,
        dataSource: updatedDataSourceRun,
      };

      case RUN_MULTI:
        let updatedDataSourceMultiRun = [...state.dataSource];
        action.jobIdList.forEach((jobId) => {
          let dataItemIndexMultiRun = updatedDataSourceMultiRun.findIndex(
            (item) => item.id === jobId
          );
          updatedDataSourceMultiRun[
            dataItemIndexMultiRun
          ].lastExecution = Moment().format(TIME_STRING);
        });
        return {
          ...state,
          dataSource: updatedDataSourceMultiRun,
        };

    case DELETE:
      let updatedDataSourceDelete = [...state.dataSource];
      const dataItemIndexDelete = updatedDataSourceDelete.findIndex(
        (item) => item.id === action.jobId
      );
      updatedDataSourceDelete[dataItemIndexDelete].deleted = true;
      return {
        ...state,
        dataSource: updatedDataSourceDelete,
      };

    case DELETE_MULTI:
      let updatedDataSourceMultiDelete = [...state.dataSource];
      action.jobIdList.forEach((jobId) => {
        let dataItemIndexMultiDelete = updatedDataSourceMultiDelete.findIndex(
          (item) => item.id === jobId
        );
        updatedDataSourceMultiDelete[dataItemIndexMultiDelete].deleted = true;
      });
      return {
        ...state,
        dataSource: updatedDataSourceMultiDelete,
      };

      case RESTORE:
      let updatedDataSourceRestore = [...state.dataSource];
      const dataItemIndexRestore = updatedDataSourceRestore.findIndex(
        (item) => item.id === action.jobId
      );
      updatedDataSourceRestore[dataItemIndexRestore].deleted = false;
      return {
        ...state,
        dataSource: updatedDataSourceRestore,
      };

    case RESTORE_MULTI:
      let updatedDataSourceMultiRestore = [...state.dataSource];
      action.jobIdList.forEach((jobId) => {
        let dataItemIndexMultiRestore = updatedDataSourceMultiRestore.findIndex(
          (item) => item.id === jobId
        );
        updatedDataSourceMultiRestore[dataItemIndexMultiRestore].deleted = false;
      });
      return {
        ...state,
        dataSource: updatedDataSourceMultiRestore,
      };

    case EDIT_PREDEFINED:
      let updatedDataSourceEditPredefined = [...state.dataSource];
      const dataItemIndexEditPredefined = updatedDataSourceEditPredefined.findIndex(
        (item) => item.id === action.jobId
      );
      updatedDataSourceEditPredefined[dataItemIndexEditPredefined].recurrence =
        action.recurrence;
      return {
        ...state,
        dataSource: updatedDataSourceEditPredefined,
      };

    case EDIT_CUSTOM:
      let updatedDataSourceEditCustom = [...state.dataSource];
      const dataItemIndexEditCustom = updatedDataSourceEditCustom.findIndex(
        (item) => item.id === action.jobId
      );
      let updateItem = updatedDataSourceEditCustom[dataItemIndexEditCustom];
      updateItem.recurrence = action.datetime;
      return {
        ...state,
        dataSource: updatedDataSourceEditCustom,
      };
      
      case CREATE_NEW:
        console.log(action.data)
        const newJob = {
          ...action.data,
          id: Date.now(),
          creationTime: Moment().format(TIME_STRING),
          lastExecution: action.run ? Moment().format(TIME_STRING) : '',
          deleted: false
        }
        return {
          ...state,
          dataSource: state.dataSource.concat(newJob)
        }

    default:
      return state;
  }
};

export default reducer;
