export const DURATION_MAP = {
    "Every Hour": 'hour',
    "Every Day": "day",
    "Every Week": "week",
    "Every Month": "month",
    "Every Year": "year"
}

export const TIME_STRING = "DD-MM-YYYY hh:mm:ss"

export const RUN = "RUN"
export const RUN_MULTI = "RUN_MULTI"
export const DELETE = "DELETE"
export const DELETE_MULTI = "DELETE_MULTI"
export const RESTORE = "RESTORE"
export const RESTORE_MULTI = "RESTORE_MULTI"
export const EDIT_PREDEFINED = "EDIT_PREDEFINED"
export const EDIT_CUSTOM = "EDIT_CUSTOM"
export const CREATE_NEW = "CREATE_NEW" 

export const runAction = (id) => {
    return {
        type: RUN,
        jobId: id
    }
}

export const runMultiAction = (idList) => {
    return {
        type: RUN_MULTI,
        jobIdList: idList
    }
}

export const deleteAction = (id) => {
    return {
        type: DELETE,
        jobId: id
    }
}

export const deleteMultiAction = (idList) => {
    return {
        type: DELETE_MULTI,
        jobIdList: idList
    }
}

export const restoreAction = (id) => {
    return {
        type: RESTORE,
        jobId: id
    }
}

export const restoreMultiAction = (idList) => {
    return {
        type: RESTORE_MULTI,
        jobIdList: idList
    }
}

export const editPredefinedAction = (id, recurrence) => {
    return {
        type: EDIT_PREDEFINED,
        jobId: id,
        recurrence: recurrence
    }
}

export const editCustomAction = (id, date, datetime) => {
    return {
        type: EDIT_CUSTOM,
        jobId: id,
        date: date,
        datetime: datetime
    }
}

export const createNewCroneJob = (jobData, runNow) => {
    return {
        type: CREATE_NEW,
        data: jobData,
        run: runNow
    }
}