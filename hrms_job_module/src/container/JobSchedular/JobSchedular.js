import React, { useState } from "react";
import { Typography, Button, Space, message } from "antd";
import { UploadOutlined, CopyFilled } from "@ant-design/icons";

import JobListing from "../../components/JobListing/JobListing";
import BackupJobListing from "../../components/BackupJobListing/BackupJobListing";
import JobCreation from "../../components/JobCreation/JobCreation";

const { Title } = Typography;

const JobSchedular = (props) => {
  const [backup, setBackup] = useState(false);

  const onBackuphandler = () => {
      setBackup(!backup)
  }

  return (
    <div>
      <Title level={2} style={{ margin: "5px 0 2px 20px" }}>
        Cron Job Schedular
      </Title>
      <Space size="middle">
        <JobCreation />
        <Button
          type="default"
          style={{ color: "white", background: "#29a329" }}
          onClick={() => {message.success("Data is Uploaded to server")}}
        >
          <UploadOutlined /> Save to Sever
        </Button>
        <Button
          type="default"
          style={{ color: "white", background: "#ff9900" }}
          onClick={onBackuphandler}
        >
          <CopyFilled /> Backup
        </Button>
      </Space>
      {backup ? <BackupJobListing className="CustomTable"/> : <JobListing className="CustomTable"/>}
    </div>
  );
};

export default JobSchedular;
