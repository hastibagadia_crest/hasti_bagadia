import React from "react";

import JobSchedular from "./container/JobSchedular/JobSchedular"

import "./App.css";

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <JobSchedular />
      </div>
    );
  }
}

export default App;
